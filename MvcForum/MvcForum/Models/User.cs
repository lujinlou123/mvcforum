﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.Models
{
    public class User
    {

        public int UserId { get; set; }
        [MaxLength(50)]
        public string UserName { get; set; }
        public string Email { get; set; }
        public string photo{ get ; set ; }
        

        public List<UserForum> UserForums { get; set; }

        public List<Post> Posts { get; set; }
    }
}
