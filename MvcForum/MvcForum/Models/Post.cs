﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.Models
{
    public class Post
    {


        public int PostId { get; set; }
        public string HeadLine { get; set; }
        public string Link { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int ForumId { get; set; }
        public Forum Forum { get; set; }

        public List<PostTag> PostTags { set; get; }
    }
}
