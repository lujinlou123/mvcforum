﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.Models
{
    public class UserForum
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int ForumId { get; set; }
        public Forum Forum { get; set; }
    }
}
