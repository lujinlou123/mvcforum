﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvcForum.Models;
using Microsoft.EntityFrameworkCore;

namespace MvcForum.Models
{
    public class ForumContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Forum> Forum1s { get; set; }
        public DbSet<UserForum> UserForum { get; set; }
        public DbSet<PostTag> PostTag { get; set; }
        public ForumContext(DbContextOptions<ForumContext> options)
     : base(options)
        {
        }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<PostTag>()
                .HasKey(t => new { t.PostId, t.TagId });

            modelBuilder.Entity<PostTag>()
                .HasOne(pt => pt.Post)
                .WithMany(p => p.PostTags)
                .HasForeignKey(pt => pt.PostId);

            modelBuilder.Entity<PostTag>()
                .HasOne(pt => pt.Tag)
                .WithMany(t => t.PostTags)
                .HasForeignKey(pt => pt.TagId);


            modelBuilder.Entity<UserForum>()
              .HasKey(a => new { a.UserId, a.ForumId });

            modelBuilder.Entity<UserForum>()
                    .HasOne(uf => uf.User)
                    .WithMany(u => u.UserForums)
                    .HasForeignKey(uf => uf.UserId);

            modelBuilder.Entity<UserForum>()
                .HasOne(uf => uf.Forum)
                .WithMany(f => f.UsersForums)
                .HasForeignKey(uf => uf.ForumId);




        }

    }

}
