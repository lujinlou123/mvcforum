﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using System;
//using System.Net.Http;
//using System.Net.Http.Headers;

//namespace MvcForum.Models
//{
//    public class UserClient
//    {
//        private string Base_URL = "http://localhost:44353/api/ABC";

//        public IEnumerable<User> findAll()
//        {
//            try
//            {
//                HttpClient client = new HttpClient();
//                client.BaseAddress = new Uri(Base_URL);
//                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
//                HttpResponseMessage response = client.GetAsync("users").Result;

//                if (response.IsSuccessStatusCode)
//                    return response.Content.ReadAsAsync<IEnumerable<User>>().Result;
//                return null;
//            }
//            catch
//            {
//                return null;
//            }
//        }
//        public User find(int id)
//        {
//            try
//            {
//                HttpClient client = new HttpClient();
//                client.BaseAddress = new Uri(Base_URL);
//                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
//                HttpResponseMessage response = client.GetAsync("users/" + id).Result;

//                if (response.IsSuccessStatusCode)
//                    return response.Content.ReadAsAsync<User>().Result;
//                return null;
//            }
//            catch
//            {
//                return null;
//            }
//        }
//        public bool Create(User user)
//        {
//            try
//            {
//                HttpClient client = new HttpClient();
//                client.BaseAddress = new Uri(Base_URL);
//                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
//                HttpResponseMessage response = client.PostAsJsonAsync("user", user).Result;
//                return response.IsSuccessStatusCode;
//            }
//            catch
//            {
//                return false;
//            }
//        }
//        public bool Edit(User user)
//        {
//            try
//            {
//                HttpClient client = new HttpClient();
//                client.BaseAddress = new Uri(Base_URL);
//                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
//                HttpResponseMessage response = client.PutAsJsonAsync("customers/" + user.UserId, user).Result;
//                return response.IsSuccessStatusCode;
//            }
//            catch
//            {
//                return false;
//            }
//        }
//        public bool Delete(int id)
//        {
//            try
//            {
//                HttpClient client = new HttpClient();
//                client.BaseAddress = new Uri(Base_URL);
//                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
//                HttpResponseMessage response = client.DeleteAsync("user/" + id).Result;
//                return response.IsSuccessStatusCode;
//            }
//            catch
//            {
//                return false;
//            }
//        }
//    }

//}
