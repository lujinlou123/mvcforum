﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.Models
{
    public class seeddata
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ForumContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ForumContext>>()))
            {
                if (context.Users.Any())
                {
                    return;
                }
                var users = new User[]
                {
                    new User { UserName = "aa", Email = "bb@bb" },
                    new User { UserName = "bb", Email = "cc@bb" }
                };

                foreach (User u in users)
                {
                    context.Users.Add(u);
                }
                context.SaveChanges();
                var forums = new Forum[]
                {
                  new Forum { ForumName = "AAA", Abbreviation = "aa" },
                   new Forum { ForumName = "BBB", Abbreviation = "bb" }
                };

                foreach (Forum f in forums)
                {
                    context.Forum1s.Add(f);
                }
                context.SaveChanges();
                var post = new Post[]
                {
                   new Post { HeadLine = "123", Link = "www.123.com", UserId = 1, ForumId = 1 },
                    new Post { HeadLine = "321", Link = "www.123.com", UserId = 2, ForumId = 2 }
                 };

                foreach (Post p in post)
                {
                    context.Posts.Add(p);
                }
                context.SaveChanges();
                var tag = new Tag[]
                {
                    new Tag { TagName = "shine" },
                    new Tag { TagName = "funny" }
                };

                foreach (Tag t in tag)
                {
                    context.Tags.Add(t);
                }
                context.SaveChanges();



            }
        }
    }
}
