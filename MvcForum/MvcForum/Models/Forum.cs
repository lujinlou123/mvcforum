﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.Models
{
    public class Forum
    {
        public int ForumId { get; set; }
        public string ForumName { get; set; }
        public string Abbreviation { get; set; }

        public List<UserForum> UsersForums { get; set; }

        public List<Post> Posts { get; set; }
    }
}
