﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.ViewModel
{
    public class ForumVm
    {
        public int ForumId { get; set; }
        public string ForumName { get; set; }
        public List<uUserVm> users { get; set; }
        public List<uPostVm> posts { get; set; }
    }
    public class uUserVm
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
    }
}
