﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.ViewModel
{
    public class TagVm
    {
        public int TagId { set; get; }
        public string TagName { set; get; }
        public List<uuPostVm> post { set; get; }
    }
    public class uuPostVm
    {
        public int PostId { get; set; }
        public string HeadLine { get; set; }

    }

}
