﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.ViewModel
{
    public class photoViewModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public int UserId { get; set; }
        public IFormFile photo { get ; set; }
        
    }
}