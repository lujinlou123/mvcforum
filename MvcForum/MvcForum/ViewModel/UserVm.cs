﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace MvcForum.ViewModel
{
    public class UserVm
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Photo { get; set; }
        public string Email { get; set; }
        public List<uForumVm> forums { get; set; }
        public List<uPostVm> posts { get; set; }


    }
    public class uPostVm
    {
        public int PostId { get; set; }
        public string HeadLine { get; set; }
        public string Link { get; set; }
    }
    public class uForumVm
    {
        public int FId { get; set; }
        public string FName { get; set; }

    }


}
