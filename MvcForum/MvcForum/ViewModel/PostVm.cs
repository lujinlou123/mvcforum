﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.ViewModel
{
    public class PostVm
    {
        public int PostId { get; set; }
        public string HeadLine { get; set; }
        public string Link { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int ForumId { get; set; }
        public string ForumName { get; set; }

        public List<uTagVm> tags { get; set; }
    }



    public class uTagVm
    {
        public int TagId { set; get; }
        public string TagName { set; get; }
    }

}
