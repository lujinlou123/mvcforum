﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.ViewModel
{
    public class ForumUserData
    {
        public int ForumID { get; set; }
        public string ForumName { get; set; }
        public bool Assigned { get; set; }
    }
}
