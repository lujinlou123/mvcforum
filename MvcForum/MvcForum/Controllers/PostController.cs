﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MvcForum.Models;
using MvcForum.ViewModel;

namespace MvcForum.Controllers
{
    [Route("api/ABCDEF")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly ForumContext _context;

        public PostController(ForumContext context)
        {
            _context = context;

            if (_context.Posts.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.Posts.Add(new Post { HeadLine = "zhangwei", Link = "123@123.com", UserId = 1, ForumId = 1 });
                _context.SaveChanges();
            }

        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PostVm>>> GetPost()
        {
            var post = _context.Posts

               .Include(p => p.Forum)
               .Include(p => p.User)
               .OrderBy(p => p.PostId)
               .Select(p => new PostVm
               {
                   PostId = p.PostId,
                   HeadLine = p.HeadLine,
                   Link = p.Link,
                   UserId = p.User.UserId,
                   UserName = p.User.UserName,
                   ForumId = p.Forum.ForumId,
                   ForumName = p.Forum.ForumName,

                   tags = p.PostTags.Select(t => new uTagVm { TagId = t.Tag.TagId, TagName = t.Tag.TagName }).ToList()

               })
               .ToList();

            return post;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PostVm>> GetPost(int id)
        {
            var item = await _context.Posts.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            else
            {

                var post = _context.Posts
                    .Include(u => u.Forum)
                   .Include(u => u.User)
                   .OrderBy(u => u.PostId)
                   .Select(p => new PostVm
                   {
                       PostId = p.PostId,
                       HeadLine = p.HeadLine,
                       Link = p.Link,
                       UserId = p.User.UserId,
                       UserName = p.User.UserName,
                       ForumId = p.Forum.ForumId,
                       ForumName = p.Forum.ForumName,

                       tags = p.PostTags.Select(t => new uTagVm { TagId = t.Tag.TagId, TagName = t.Tag.TagName }).ToList()

                   })
                   .ToList();


                return post.Find(u => u.PostId == id);
            }

        }
        [HttpPost]
        public async Task<ActionResult<Post>> PostPost(Post post)
        {
            //_context.Posts.Add(post);
            //await _context.SaveChangesAsync();

            //return CreatedAtAction(nameof(PostPost), new { id = post.PostId }, post);

            var user = _context.Users
                .Include(u => u.Posts)
                .Where(u => u.UserId == post.UserId).First();
            //var forum = _context.Forum1s
            //    .Include(f => f.Posts)
            //    .Where(f => f.ForumId == post.ForumId).First();


            user.Posts.Add(post);
            //forum.Posts.Add(post);
            _context.SaveChanges();

            return CreatedAtAction(nameof(PostPost), new { id = user.UserId }, post);
        }



        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            var item = await _context.Posts.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            _context.Posts.Remove(item);
            await _context.SaveChangesAsync();


            return NoContent();
        }



        [HttpPut("{id}")]
        public async Task<IActionResult> PutPost(int id, Post post)
        {
            if (id != post.PostId)
            {
                return BadRequest();
            }
            var postlist1 = new HashSet<int>(_context.PostTag
                .Where(pt => pt.PostId == id)
                .Select(pt => pt.TagId));
            var postlist2 = new HashSet<int>(post.PostTags.Select(pt => pt.TagId));
            foreach (var oldid in postlist1)
            {
                if (!postlist2.Contains(oldid))
                {
                    _context.PostTag.Remove(new PostTag { PostId = id, TagId = oldid });
                }
            }
            foreach (var newid in postlist2)
            {
                if (!postlist1.Contains(newid))
                {
                    _context.PostTag.Add(new PostTag { PostId = id, TagId = newid });

                }
            }
            _context.Entry(post).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }


    }

}