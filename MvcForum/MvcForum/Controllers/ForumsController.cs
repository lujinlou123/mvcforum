﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcForum.Models;
using MvcForum.ViewModel;

namespace MvcForum.Controllers
{
    public class ForumsController : Controller
    {
        private readonly ForumContext _context;

        public ForumsController(ForumContext context)
        {
            _context = context;
        }

        // GET: Forums
        public async Task<IActionResult> Index(String searchstring)
        {
            var forums = from f in _context.Forum1s
                        select f;
            if (!String.IsNullOrEmpty(searchstring))
            {
                forums = forums.Where(s => s.ForumName.Contains(searchstring));
            }



            return View(await forums.ToListAsync());
        }

        // GET: Forums/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var forum = _context.Forum1s
                             .Include(f => f.UsersForums)
                             .Include(f => f.Posts)
                             .OrderBy(f => f.ForumId)
                          .Select(f => new ForumVm
                          {
                              ForumId = f.ForumId,
                              ForumName = f.ForumName,
                              posts = f.Posts.Select(p => new uPostVm
                              {
                                  PostId = p.PostId,
                                  HeadLine = p.HeadLine,
                                  Link = p.Link

                              }).ToList(),
                              users = f.UsersForums.Select(u => new uUserVm { UserName = u.User.UserName, UserId = u.User.UserId }).ToList()
                          }).FirstOrDefault(m => m.ForumId == id);
                             
                
               
            if (forum == null)
            {
                return NotFound();
            }

            return View(forum);
        }

        // GET: Forums/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Forums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ForumId,ForumName,Abbreviation")] Forum forum)
        {
            if (ModelState.IsValid)
            {
                _context.Add(forum);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(forum);
        }

        // GET: Forums/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var forum = await _context.Forum1s.FindAsync(id);
            if (forum == null)
            {
                return NotFound();
            }
            return View(forum);
        }

        // POST: Forums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ForumId,ForumName,Abbreviation")] Forum forum)
        {
            if (id != forum.ForumId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(forum);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ForumExists(forum.ForumId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(forum);
        }

        // GET: Forums/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var forum = await _context.Forum1s
                .FirstOrDefaultAsync(m => m.ForumId == id);
            if (forum == null)
            {
                return NotFound();
            }

            return View(forum);
        }

        // POST: Forums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var forum = await _context.Forum1s.FindAsync(id);
            _context.Forum1s.Remove(forum);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ForumExists(int id)
        {
            return _context.Forum1s.Any(e => e.ForumId == id);
        }
    }
}
