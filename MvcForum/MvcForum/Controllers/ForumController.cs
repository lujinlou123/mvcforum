﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MvcForum.Models;
using MvcForum.ViewModel;

namespace MvcForum.Controllers
{
    [Route("api/ABCD")]
    [ApiController]
    public class ForumController : ControllerBase
    {
        private readonly ForumContext _context;

        public ForumController(ForumContext context)
        {
            _context = context;

            if (_context.Forum1s.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.Forum1s.Add(new Forum { ForumName = "AAA", Abbreviation = "aa" });
                _context.SaveChanges();
            }

        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ForumVm>>> GetForum()
        {
            var forum = _context.Forum1s
              .Include(f => f.UsersForums)
              .Include(f => f.Posts)
              .OrderBy(f => f.ForumId)
               .Select(f => new ForumVm
               {
                   ForumId = f.ForumId,
                   ForumName = f.ForumName,
                   posts = f.Posts.Select(p => new uPostVm
                   {
                       PostId = p.PostId,
                       HeadLine = p.HeadLine,
                       Link = p.Link

                   }).ToList(),
                   users = f.UsersForums.Select(u => new uUserVm { UserName = u.User.UserName, UserId = u.User.UserId }).ToList()
               })
               .ToList();

            return forum;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ForumVm>> GetForum(int id)
        {

            var item = await _context.Forum1s.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            else
            {
                var forum = _context.Forum1s
                  .Include(f => f.UsersForums)
                  .Include(f => f.Posts)
                  .OrderBy(f => f.ForumId)
               .Select(f => new ForumVm
               {
                   ForumId = f.ForumId,
                   ForumName = f.ForumName,
                   posts = f.Posts.Select(p => new uPostVm
                   {
                       PostId = p.PostId,
                       HeadLine = p.HeadLine,
                       Link = p.Link

                   }).ToList(),
                   users = f.UsersForums.Select(u => new uUserVm { UserName = u.User.UserName, UserId = u.User.UserId }).ToList()
               })
                  .ToList();


                return forum.Find(u => u.ForumId == id);
            }
        }
        [HttpPost]
        public async Task<ActionResult<Forum>> PostForum(Forum forum)
        {


            _context.Forum1s.Add(forum);

            //_context.UserForum.Add(forum.UsersForums.First());

            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetForum), new { id = forum.ForumId }, forum);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem(int id)
        {
            var item = await _context.Forum1s.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            _context.Forum1s.Remove(item);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutForumItem(int id, Forum item)
        {
            if (id != item.ForumId)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }


    }
}
