﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvcForum.Models;
using MvcForum.ViewModel;

namespace MvcForum.Controllers
{
    [Route("api/ABC")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ForumContext _context;

        public UserController(ForumContext context)
        {
            _context = context;

            if (_context.Users.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.Users.Add(new User { UserName = "zhangwei", Email = "123@123.com"});
                _context.SaveChanges();
            }


        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserVm>>> GetUser()
        {

            var user = _context.Users
               //.Include(u=>u.UserForums)
               .Include(u => u.Posts)
               .OrderBy(u => u.UserId)
               .Select(u => new UserVm
               {
                   UserId = u.UserId,
                   UserName = u.UserName,
                   posts = u.Posts.Select(p => new uPostVm
                   {
                       PostId = p.PostId,
                       HeadLine = p.HeadLine,
                       Link = p.Link,

                   }).ToList(),
                   forums = u.UserForums.Select(f => new uForumVm { FName = f.Forum.ForumName }).ToList()

               })
               .ToList();

            return user;

        }
        [HttpGet("{id}")]
        public async Task<ActionResult<UserVm>> GetUser(int id)
        {
            var item = await _context.Users.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            else
            {
                var user = _context.Users
                   .Include(u => u.Posts)
                   .OrderBy(u => u.UserId)
               .Select(u => new UserVm
               {
                   UserId = u.UserId,
                   UserName = u.UserName,
                   posts = u.Posts.Select(p => new uPostVm
                   {
                       PostId = p.PostId,
                       HeadLine = p.HeadLine
                   }).ToList(),
                   forums = u.UserForums.Select(f => new uForumVm { FName = f.Forum.ForumName }).ToList()
               })
                   .ToList();
                return user.Find(u => u.UserId == id);
            }
        }
        [HttpPost]
        //创建
        public async Task<ActionResult<User>> PostUser(User user)
        {
            //var userforum = _context.UserForums
            //    .Include(u => u.User)
            //    .Where(u => u.UserId == user.UserId).First();

            _context.UserForum.Add(user.UserForums.First());

            _context.Users.Add(user);
            //_context.UserForums.Add(user.UserForums.First());

            await _context.SaveChangesAsync();
            return CreatedAtAction(nameof(PostUser), new { id = user.UserId }, user);
            //  _context.Users.Add(item);
            //await _context.SaveChangesAsync();
            //return CreatedAtAction(nameof(PostUser), new { id = item.UserId}, item);

        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem(int id)
        {
            var item = await _context.Users.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }
            _context.Users.Remove(item);
            await _context.SaveChangesAsync();
            return NoContent();
        }
        [HttpPut("{id}")]
        //修改
        public async Task<ActionResult<User>> PutUser(int id, User user)
        {
            if (id != user.UserId)
            {
                return BadRequest();
            }
            var userlist1 = new HashSet<int>(_context.UserForum
                .Where(uf => uf.UserId == id)
                .Select(uf => uf.ForumId));
            var userlist2 = new HashSet<int>(user.UserForums.Select(uf => uf.ForumId));
            foreach (var oldid in userlist1)
            {

                if (!userlist2.Contains(oldid))
                    _context.UserForum.Remove(new UserForum { UserId = id, ForumId = oldid });

            }
            foreach (var newid in userlist2)
            {
                if (!userlist1.Contains(newid))
                    _context.UserForum.Add(new UserForum { UserId = id, ForumId = newid });
            }


            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();

        }

    }

}