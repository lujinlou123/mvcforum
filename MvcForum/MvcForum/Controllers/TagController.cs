﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MvcForum.Models;
using MvcForum.ViewModel;

namespace MvcForum.Controllers
{
    [Route("api/ABCDE")]
    [ApiController]
    public class TagController : ControllerBase
    {
        private readonly ForumContext _context;

        public TagController(ForumContext context)
        {
            _context = context;

            if (_context.Tags.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.Tags.Add(new Tag { TagName = "happy" });
                _context.SaveChanges();
            }

        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TagVm>>> GetTag()
        {
            var tag = _context.Tags
                  .Include(t => t.PostTags)
                 .OrderBy(t => t.TagId)
                   .Select(t => new TagVm
                   {
                       TagId = t.TagId,
                       TagName = t.TagName,
                       post = t.PostTags.Select(f => new uuPostVm { PostId = f.Post.PostId, HeadLine = f.Post.HeadLine }).ToList()
                   })
                 .ToList();
            return tag;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TagVm>> GetTag(int id)
        {

            var item = await _context.Tags.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }
            else
            {

                var tag = _context.Tags
                     .Include(t => t.PostTags)
                    .OrderBy(u => u.TagId)
                   .Select(t => new TagVm
                   {
                       TagId = t.TagId,
                       TagName = t.TagName,
                       post = t.PostTags.Select(f => new uuPostVm { PostId = f.Post.PostId, HeadLine = f.Post.HeadLine }).ToList()
                   })
                    .ToList();


                return tag.Find(t => t.TagId == id);
            }


        }
        [HttpPost]
        public async Task<ActionResult<Tag>> PostTag(Tag tag)
        {
            _context.PostTag.Add(tag.PostTags.First());

            _context.Tags.Add(tag);

            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetTag), new { id = tag.TagId }, tag);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTag(int id)
        {
            var item = await _context.Tags.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            _context.Tags.Remove(item);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTag(int id, Tag tag)
        {
            if (id != tag.TagId)
            {
                return BadRequest();
            }


            _context.Entry(tag).State = EntityState.Modified;


            await _context.SaveChangesAsync();

            return NoContent();
        }


    }
}
