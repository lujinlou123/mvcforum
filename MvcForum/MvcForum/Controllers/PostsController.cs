﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcForum.Models;
using MvcForum.ViewModel;

namespace MvcForum.Controllers
{
    public class PostsController : Controller
    {
        private readonly ForumContext _context;

        public PostsController(ForumContext context)
        {
            _context = context;
        }

        // GET: Posts
        public async Task<IActionResult> Index(String searchstring )
        {
            var posts = from p in _context.Posts

               .Include(p => p.Forum)
               .Include(p => p.User)
                        select p;
            if (!String.IsNullOrEmpty(searchstring))
            {
                posts = posts.Where(s => s.HeadLine.Contains(searchstring));
            }

            return View(await posts.ToListAsync());          
            
        }

        // GET: Posts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var post = _context.Posts

               .Include(p => p.Forum)
               .Include(p => p.User)
               .OrderBy(p => p.PostId)
               .Select(p => new PostVm
               {
                   PostId = p.PostId,
                   HeadLine = p.HeadLine,
                   Link = p.Link,
                   UserId = p.User.UserId,
                   UserName = p.User.UserName,
                   ForumId = p.Forum.ForumId,
                   ForumName = p.Forum.ForumName,

                   tags = p.PostTags.Select(t => new uTagVm { TagId = t.Tag.TagId, TagName = t.Tag.TagName }).ToList()

               }).FirstOrDefault(p=>p.PostId==id);


            if ( post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // GET: Posts/Create
        public IActionResult Create()
        {
            var post = new Post();
            post.PostTags = new List<PostTag>();
            PopulatePostTagData(post);
            ViewData["ForumId"] = new SelectList(_context.Forum1s, "ForumId", "ForumId");
            ViewData["UserId"] = new SelectList(_context.Users, "UserId", "UserId");
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PostId,HeadLine,Link,UserId,ForumId")] Post post,string[] selectedTags)
        {
            if (selectedTags != null)
            {
                post.PostTags = new List<PostTag>();
                foreach (var tag in selectedTags)
                {
                    var tagToAdd = new PostTag { PostId = post.PostId, TagId= int.Parse(tag) };
                    post.PostTags.Add(tagToAdd);
                }
            }
            if (ModelState.IsValid)
            {
                _context.Add(post);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ForumId"] = new SelectList(_context.Forum1s, "ForumId", "ForumId", post.ForumId);
            ViewData["UserId"] = new SelectList(_context.Users, "UserId", "UserId", post.UserId);
            PopulatePostTagData(post);
            return View(post);
        }

        // GET: Posts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            //var post = await _context.Posts.FindAsync(id);
            var post = await _context.Posts
              .Include(u => u.PostTags)
              .ThenInclude(u=>u.Tag)
              .AsNoTracking()
              .SingleOrDefaultAsync(m => m.PostId == id);
            if (post == null)
            {
                return NotFound();
            }
            PopulatePostTagData(post);
            ViewData["ForumId"] = new SelectList(_context.Forum1s, "ForumId", "ForumId", post.ForumId);
            ViewData["UserId"] = new SelectList(_context.Users, "UserId", "UserId", post.UserId);
            return View(post);
        }
        private void PopulatePostTagData(Post post)
        {
            var allTag = _context.Tags;
            var postPost = new HashSet<int>(post.PostTags.Select(uf => uf.TagId));
            var viewmodel = new List<PostTagData>();
            foreach (var tag in allTag)
            {
                viewmodel.Add(new PostTagData
                {
                     TagID=tag.TagId,
                      TagName=tag.TagName,
                    Assigned = postPost.Contains(tag.TagId)
                });
            }
            ViewData["Tags"] = viewmodel;
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, string[] selectedtags)
        {
            if (id == null)
            {
                return NotFound();
            }

            var postsToUpdate = await _context.Posts
                .Include(i => i.PostTags)
                .ThenInclude(i => i.Tag)
                .SingleOrDefaultAsync(i => i.PostId == id);
            if (await TryUpdateModelAsync<Post>(
                postsToUpdate,
                "",
                i => i.HeadLine, i => i.Link,i=>i.UserId,i=>i.ForumId,i=>i.PostTags))
            {

                UpdatePostTag(selectedtags, postsToUpdate);
                try
                {
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateException)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
                return RedirectToAction(nameof(Index));

            }
            UpdatePostTag(selectedtags, postsToUpdate);
            PopulatePostTagData(postsToUpdate);
            ViewData["ForumId"] = new SelectList(_context.Forum1s, "ForumId", "ForumId", postsToUpdate.ForumId);
            ViewData["UserId"] = new SelectList(_context.Users, "UserId", "UserId", postsToUpdate.UserId);        
            return View(postsToUpdate);
        }
        private void UpdatePostTag(string[] selectedTags, Post  postsToUpdate)
        {
            if (selectedTags == null)
            {
                postsToUpdate.PostTags = new List<PostTag>();
                return;
            }
            var selectedTagsHs = new HashSet<string>(selectedTags);
            var postTags = new HashSet<int>
                (postsToUpdate.PostTags.Select(u => u.Tag.TagId));
            foreach (var tag in _context.Tags)
            {
                if (selectedTagsHs.Contains(tag.TagId.ToString()))
                {
                    if (!postTags.Contains(tag.TagId))
                    {
                        postsToUpdate.PostTags.Add(new PostTag { PostId = postsToUpdate.PostId, TagId = tag.TagId});
                    }
                }
                else
                {
                    if (postTags.Contains(tag.TagId))
                    {
                        PostTag tagToRemove = postsToUpdate.PostTags.SingleOrDefault(i => i.TagId == tag.TagId);
                        _context.Remove(tagToRemove);
                    }
                }
            }
        }
      
        

        // GET: Posts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Posts
                .Include(p => p.Forum)
                .Include(p => p.User)
                .FirstOrDefaultAsync(m => m.PostId == id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var post = await _context.Posts.FindAsync(id);
            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PostExists(int id)
        {
            return _context.Posts.Any(e => e.PostId == id);
        }
    }
}
