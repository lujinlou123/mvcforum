﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MvcForum.Models;
using MvcForum.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MvcForum.Controllers
{
    public class UsersController : Controller
    {
        private readonly IHostingEnvironment _hostenv;
        private readonly ForumContext _context;    
        public UsersController(ForumContext context , IHostingEnvironment hostenv)
        {
            _context = context;
            _hostenv = hostenv;
        }
        // GET: Users
        public async Task<IActionResult> Index(string searchstring)
        {
            var users=from u in _context.Users
                      select u;
            if (!String.IsNullOrEmpty(searchstring))
            {
                users = users.Where(s => s.UserName.Contains(searchstring));
            }

            return View(await  users.ToListAsync());
        }


        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = _context.Users
               //.Include(u=>u.UserForums)
               .Include(u => u.Posts)
               .OrderBy(u => u.UserId)
               .Select(u => new UserVm
               {
                   UserId = u.UserId,
                   UserName = u.UserName,
                   Email=u.Email,
                   Photo = u.photo,
                   posts = u.Posts.Select(p => new uPostVm
                   {
                       PostId = p.PostId,
                       HeadLine = p.HeadLine,
                       Link = p.Link,

                   }).ToList(),
                   forums = u.UserForums.Select(f => new uForumVm { FName = f.Forum.ForumName,FId=f.Forum.ForumId}).ToList()
               }).FirstOrDefault(u => u.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }
        // GET: Users/Create
        public IActionResult Create()
        {
            var user = new User();
            user.UserForums = new List<UserForum>();
            PopulateUserForumData(user);
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,UserName,Email,photo")] User user,string[] selectedForums,photoViewModel image)
        { 

            if (selectedForums != null)
            {
                user.UserForums = new List<UserForum>();
                foreach (var forum in selectedForums)
                {
                    var forumToAdd = new UserForum { UserId = user.UserId, ForumId = int.Parse(forum) };
                    user.UserForums.Add(forumToAdd);
                }
            }
            if (ModelState.IsValid)
            {
                
                var imageName = Path.GetFileName(image.photo.FileName);
                var imagePath = Path.Combine(_hostenv.WebRootPath, "image",imageName);
                if (image.photo.FileName.Length > 0)
                {
                    using (var filestream = new FileStream(imagePath, FileMode.Create))
                    {
                        await image.photo.CopyToAsync(filestream);
                        user.photo = imagePath.ToString();
                    }
                }

                _context.Add(user);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            PopulateUserForumData(user);
            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id==null)
            {
                return NotFound();
            }
            var users = await _context.Users          
                .Include(u => u.UserForums)
                .AsNoTracking()
                .SingleOrDefaultAsync(m => m.UserId == id);
            if (users == null)
            {
                return NotFound();

            }
            PopulateUserForumData(users);
            return View(users);
        }
        private void PopulateUserForumData(User user)
        {
            var allForum = _context.Forum1s;
            var userUser = new HashSet<int>(user.UserForums.Select(uf => uf.ForumId));
            var viewmodel = new List<ForumUserData>();
            foreach (var forum in allForum)
            {
                viewmodel.Add(new ForumUserData
                {
                    ForumID = forum.ForumId,
                    ForumName = forum.ForumName,
                    Assigned = userUser.Contains(forum.ForumId)
                });
            }
            ViewData["Forums"] = viewmodel;
        }


        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int? id, string[] selectedforums)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usersToUpdate = await _context.Users
                .Include(i => i.UserForums)
                .ThenInclude(i=>i.Forum)
                .SingleOrDefaultAsync(i => i.UserId == id);
            if (await TryUpdateModelAsync<User>(
                usersToUpdate,
                "",
                i => i.UserName, i => i.Email, i => i.photo))
            {

                UpdateUserForums(selectedforums, usersToUpdate);
                try
                {
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateException )
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
                return RedirectToAction(nameof(Index));

            }
            UpdateUserForums(selectedforums, usersToUpdate);
            PopulateUserForumData(usersToUpdate);
            return View(usersToUpdate);
        }
        private void UpdateUserForums(string[] selectedforums, User userToUpdate)
        {
            if (selectedforums == null)
            {
                 userToUpdate.UserForums= new List<UserForum>();
                return;
            }
            var selectedForumsHs = new HashSet<string>(selectedforums);
            var userForums = new HashSet<int>
                (userToUpdate.UserForums.Select(u => u.Forum.ForumId));
            foreach (var forum in _context.Forum1s)
            {
                if (selectedForumsHs.Contains(forum.ForumId.ToString()))
                {
                    if (!userForums.Contains(forum.ForumId))
                    {
                        userToUpdate.UserForums.Add(new UserForum { UserId = userToUpdate.UserId, ForumId = forum.ForumId });
                    }
                }
                else
                {
                    if (userForums.Contains(forum.ForumId))
                    {
                        UserForum forumToRemove = userToUpdate.UserForums.SingleOrDefault(i => i.ForumId == forum.ForumId);
                        _context.Remove(forumToRemove);
                    }
                }
            }
        }


        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.Users
                .FirstOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await _context.Users.FindAsync(id);
            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.UserId == id);
        }
    }
}
